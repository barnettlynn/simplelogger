package simpleLogger

import (
	"log"
	"os"
)

// TODO: ROLLING LOG BASED ON SIZE AND BASED ON DATE
// TODO: LOG FILE BASED ON APPLICATION AND BASED ON DATE
// TODO: CHANGE MAIN TO INIT/CONTRUCTOR/FACTORY THAT THROWS AN ERROR ISTED PANIC SO IT CAN BE BUBBLE UP TO APP USING PACKAGE
// TODO: CREATE LOG FILE PARSER
// TODO: COLORED LOGGIN
// TODO: DEFAULT LOCATION AND FILE NAME THAT CAN BE OVERWRITTEN

type SimpleLogger struct {
	LogDirectory string
	LogFileName  string
	File         *os.File
	InfoLogger   *log.Logger
	WarnLogger   *log.Logger
}

func (sl *SimpleLogger) Info(message string) {
	sl.InfoLogger.Println(message)
}

func (sl *SimpleLogger) Warn(message string) {
	sl.WarnLogger.Println(message)
}

func New() *SimpleLogger {

	lg := new(SimpleLogger)
	lg.LogDirectory = "./logs"
	lg.LogFileName = "SimpleLoggerLog"

	ex, err := exists(lg.LogDirectory)
	if err != nil {
		log.Panic(err)
	}
	// TODO: reverse for one level of direction
	if ex { // If Directory Exists
		// Do Nothing
	} else { // If Folder Exists
		err := os.Mkdir(lg.LogDirectory, 0777)
		if err != nil {
			log.Panic(err)
		}
	}

	logfile, err := os.OpenFile(lg.LogDirectory+"/"+lg.LogFileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		log.Panic(err)
	}
	// TODO: close file at some point
	lg.File = logfile

	lg.InfoLogger = log.New(logfile, "INFO: ", log.Ldate|log.Ltime)
	lg.WarnLogger = log.New(logfile, "WARN: ", log.Ldate|log.Ltime)

	return lg

}

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
